<?php
include 'ScoreDataIndexerInterface.php';
define("TITLE_COLUMNS", 0);
define("GENDER", 1);
define("AGE", 2);
define("REGION", 3);
define("SCORE", 4);
define("LEGAL_AGE", 18);
class Statistics implements ScoreDataIndexerInterface
{

	public $datos = [];
	public function __construct($fileName)
	{
		$data = str_getcsv(file_get_contents($fileName), "\n"); 
		foreach ($data as $row) {
			$row = str_getcsv($row, ";");
			$this->datos[] = $row;
		}
		unset($this->datos[TITLE_COLUMNS]);
	}

	public function getCountOfUsersWithinScoreRange(int $rangeStart, int $rangeEnd): int
	{
		$count = 0;
		foreach ($this->datos as $row) {
			$count += $row[SCORE] >= $rangeStart && $row[SCORE] <= $rangeEnd ? 1 : 0;
		}
		return $count;
	}

	public function getCountOfUsersByCondition(string $region,  string $gender,  bool $hasLegalAge,  bool $hasPositiveScore): int
	{
		$count  = 0;
		foreach ($this->datos as $row) {
			$count += (
						$row[REGION] == $region && $row[GENDER] == $gender && 
						($hasLegalAge ? $row[AGE] >= LEGAL_AGE : $row[AGE] <= LEGAL_AGE)  && 
						($hasPositiveScore ? $row[SCORE] >= 0 : $row[SCORE] <= 0)
					) ? 1 : 0;
		}

		return $count;
	}
}

$index = new Statistics("dataPrueba.csv");

echo $index->getCountOfUsersWithinScoreRange(20, 50);
echo "<br>";
echo $index->getCountOfUsersWithinScoreRange(-40, 0);
echo "<br>";
echo $index->getCountOfUsersWithinScoreRange(0, 80);
echo "<hr>";
echo $index->getCountOfUsersByCondition('CA', 'w', false, false); // 1
echo "<br>";
echo $index->getCountOfUsersByCondition('CA', 'w', false, true); // 0
echo "<br>";
echo $index->getCountOfUsersByCondition('CA', 'w', true, true); // 1
